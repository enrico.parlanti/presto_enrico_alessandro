// Annunci JSON
// Filtri
fetch('./annunci.json')
.then(response => response.json())
.then(data => {

    let adsCopy = data.map (el => el)

    function populateCategoryFilter () {
        let catWrapper = document.querySelector('#filterCategoryWrapper')
        
        let categories = new Set (data.map (ad => ad.category))
        
        categories.forEach(category => {
            let form = document.createElement ('div')

            form.classList.add('form-check')
            form.innerHTML = `
                <input data-filter="${category}" class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                <label class="form-check-label" for="exampleRadios1">
                    ${category}
                </label>
            `

                catWrapper.appendChild(form)
        })
    }
    populateCategoryFilter()

    function populatePriceFilter (){
        let input = document.querySelector('#inputPrice')
        let setPrice = document.querySelector('#setPrice')

        let max = Math.ceil(Math.max.apply(Math , data.map (ad => Number(ad.price))))
        let min = Math.ceil(Math.min.apply(Math , data.map (ad => Number(ad.price))))
        
        document.querySelector('#maxPrice').innerHTML = max + ' €'
        document.querySelector('#minPrice').innerHTML = min + ' €'

        input.addEventListener('input' , () => {
            setPrice.innerHTML = input.value + ' €'
        })

        input.max = max
        input.min = min
        input.value = max
        setPrice.innerHTML = input.value + ' €'

        input.addEventListener('input' , () => {
            setPrice.innerHTML = input.value + ' €'
        })

    }
    populatePriceFilter()


    function populateAds (ads) {
        let adsWrapper = document.querySelector('#adsWrapper')
        
        adsWrapper.innerHTML = ''

        ads.forEach(ad => {   
            let adCard = document.createElement('div')
            adCard.classList.add('col-12', 'col-md4', 'col-lg-3', 'my-3')
            adCard.innerHTML = `
            <div class="card" style="width: 18rem;">
              <img src="https://picsum.photos/${200 + Number(ad.id)}" class="card-img-top" alt="...">
              <div class="card-body text-center">
                <h4 class="card-title text-orange font-weight-bold">${ad.name}</h4>
                <h5 class="card-title text-blue font-weight-bold">${Math.floor(ad.price)}€</h5>
                <h5 class="text-blue px-2 font-weight-bold">${ad.category}</h5>
                <a href="#" class="btn btn-search">Vedi Articolo</a>
              </div>
            </div>
            `
    
            adsWrapper.appendChild(adCard)
        })
    }
    
    populateAds(data)


    // function filterByCategory () {
    //     let categories = new Set (data.map (ad => ad.category))

    //     categories.forEach( cat => {
    //         let input = document.querySelector(`[data-filter = "${cat}"]`)

    //         input.addEventListener('input' , () => {
    //             let filter = input.getAttribute('data-filter')
    //             let filteredAds = data.filter(ad => ad.category == filter)
    //             populateAds (filteredAds)
    //         })

    //     })

    //     document.querySelector('#allAds').addEventListener('input' , () => {
    //         populateAds(data)
    //     })
    // }
    // filterByCategory()

    // function filterByPrice() {
    //     let input = document.querySelector('#inputPrice')
    //     let setPrice = document.querySelector('#setPrice')

    //     input.addEventListener('input' , () => {
    //         setPrice.innerHTML = input.value + ' €'
    //         let filteredAds = data.filter(ad => ad.price < input.value)
    //         populateAds(filteredAds)
    //     })

    // } 
    // filterByPrice()


    // function filterByWord() {
    //     let query = document.querySelector('#selectedWord')
        
    //     query.addEventListener('input' , () => {
    //         let filteredAds = data.filter(ad => ad.name.includes(query.value))
    //         populateAds(filteredAds)
            
    //     })
    // }
    
    // filterByWord()


    function filterCombo () {
        let apply = document.querySelector('#applyFilter')

        apply.addEventListener ('click' , () => {
            // Filtri
            let filteredCategory = Array.from(document.querySelectorAll('input[type="radio"]')).filter(el => el.checked == true)[0].getAttribute('data-filter')
            let filteredPrice = document.querySelector('#inputPrice').value
            let filteredWord = document.querySelector('#selectedWord').value

            // Annunci filtrati
            let filteredByPrice = data.filter(ad => Number(ad.price) < Number(filteredPrice))

            let filteredByCategory = []

            if (filteredCategory != 'Tutti') {
                filteredByCategory = filteredByPrice.filter(ad => ad.category == filteredCategory)
            } else {
                filteredByCategory = filteredByPrice.map(el => el)
            }

            let filteredByWord = filteredByCategory.filter(ad => ad.name.toLowerCase().includes(filteredWord.toLowerCase()))

            populateAds (filteredByWord)
            
        })

    }
    filterCombo()

    // Funzione tasto reset
    function clearFilter (){
        let reset = document.querySelector('#resetFilter')

        reset.addEventListener('click' , () => {
            let max = Math.ceil(Math.max.apply(Math , data.map (ad => Number(ad.price))))
            // let filteredCategory = Array.from(document.querySelectorAll('input[type="radio"]')).filter(el => el.checked == true)[0].getAttribute('data-filter')
            document.querySelector('#inputPrice').value = max
            document.querySelector('#selectedWord').value = ''
            document.querySelector('#setPrice').innerHTML = max + ' €'

            document.querySelector('[data-filter="Tutti"]').checked = true

            populateAds(data)
        })
    }
    clearFilter()
})