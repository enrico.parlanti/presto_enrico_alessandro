// Funzione tabella annunci

let myAds = [
    {
    'id' : 1,
    'name' : 'Fotocamera Polaroid',
    'price' : 230,
    'description' : 'Nice pics',
    'category' : 'Elettronica',
    'status' : true,


    },
    {
        'id' : 2,
        'name' : 'Camion dei Gelati',
        'price' : 15000,
        'description' : 'Jingle non incluso',
        'category' : 'Motori',
        'status' : true,
    
    
    },
    {
        'id' : 3,
        'name' : 'PS5',
        'price' : 499,
        'description' : 'Ti piacerebbe...',
        'category' : 'Elettronica',
        'status' : false,
    
    
    },
    {
        'id' : 4,
        'name' : 'Basso Elettrico',
        'price' : 300,
        'description' : 'SLAP LIKE NOW!!!',
        'category' : 'Musica',
        'status' : true,
    
    
    },
    {
        'id' : 5,
        'name' : 'Armatura del Sagittario',
        'price' : '?',
        'description' : 'Te la meriti?',
        'category' : 'Abbigliamento',
        'status' : false,
    
    
    },

]






function profileAds (myAds) {
    let profileWrapper = document.querySelector('#profileWrapper')

    myAds.forEach(ad => {   
        let adRow = document.createElement('tr')
        let today = new Date().toLocaleDateString().slice(0,10)
        adRow.innerHTML = `
        <tr>
        <th scope="row" class="text-orange">${ad.id}</th>
        <td class="text-truncate">${ad.name}</td>
        <td>${ad.price}€</td>
        <td class="text-truncate">${ad.description.substring(0 , 150)}</td>
        <td class="text-orange">${ad.category}</td>
        <td>
        ${ad.status ? '<i class="fas fa-check-circle mx-2 text-success"></i>' : '<i class="fas fa-times-circle mx-2 text-danger"></i>' }
        </td>
        <td class="text-truncate">${today}</td>
      </tr>
      `

        profileWrapper.appendChild(adRow)
    })
}
profileAds(myAds)