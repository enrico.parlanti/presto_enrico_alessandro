

document.addEventListener('scroll' , () => {
    let navbar = document.querySelector('#navbar-presto')
    if (window.pageYOffset > 10){
        navbar.classList.add('bg-scroll')
    }


    else{
        navbar.classList.remove('bg-scroll')
    }
})
//  lunga lista di array e funzioni per generare le card in homepage
let ads = [
    {
    'id' : 1,
    'name' : 'Samsung S20',
    'price' : 500,
    'description' : 'Veloce e affidabile',
    'category' : 'Elettronica',


    },
    {
        'id' : 2,
        'name' : 'Bilancere',
        'price' : 100,
        'description' : 'Get fit',
        'category' : 'Sport',
    
    
    },
    {
        'id' : 3,
        'name' : 'Gazebo',
        'price' : 350,
        'description' : 'Protegge dai raggi UV ma non dal Coronavirus',
        'category' : 'Articoli per la Casa'
    
    
    },
]
let moreAds = [
    {
    'id' : 4,
    'name' : 'Motorola',
    'price' : 100,
    'description' : 'Pippo Pluto e Paperino',
    'category' : 'Elettronica',


    },
    {
        'id' : 5,
        'name' : 'Gondola',
        'price' : 5000,
        'description' : 'Ostregheta',
        'category' : 'Altro',
    
    
    },
    {
        'id' : 6,
        'name' : 'Ciao',
        'price' : 500,
        'description' : 'Va a benzina e preghiere',
        'category' : 'Motori'
    
    
    },
    {
        'id' : 7,
        'name' : 'MacBook',
        'price' : 800,
        'description' : 'Usato garantito',
        'category' : 'Elettronica',
    
    
        },
        {
            'id' : 8,
            'name' : 'Palla da basket',
            'price' : 20,
            'description' : 'Rimbalza ma non troppo',
            'category' : 'Sport',
        
        
        },
        {
            'id' : 9,
            'name' : 'Grammofono',
            'price' : 200,
            'description' : 'Old but gold',
            'category' : 'Articoli per la Casa'
        
        
        },
]
function mainpageAds (ads) {
    let adsWrapper = document.querySelector('#adsWrapper')

    ads.forEach(ad => {   
        let adCard = document.createElement('div')
        // adCard.classList.add('col-12', 'col-md4', 'col-lg-3')
        adCard.innerHTML = 
        `<div class="card my-2" style="width: 18rem;">
          <img src="https://picsum.photos/${200 + Number(ad.id)}" class="card-img-top" alt="...">
          <div class="card-body text-center">
            <h4 class="card-title text-orange font-weight-bold">${ad.name}</h4>
            <h5 class="card-title text-blue font-weight-bold">${ad.price}€</h5>
            <h5 class="text-blue px-2 font-weight-bold">${ad.category}</h5>
            <p class="card-text text-blue text-truncate">${ad.description}</p>
            <a href="#" class="btn btn-search">Vedi Articolo</a>
          </div>
        </div>`

        adsWrapper.appendChild(adCard)
    })
}
mainpageAds(ads)

function mainHiddenAds (moreAds) {
    let hiddenAds = document.querySelector('#hiddenAds')

    moreAds.forEach(ad => {   
        let adCard = document.createElement('div')
        adCard.innerHTML = 
        `<div class="card my-2" style="width: 18rem;">
          <img src="https://picsum.photos/${200 + Number(ad.id)}" class="card-img-top" alt="...">
          <div class="card-body text-center">
          <h4 class="card-title text-orange font-weight-bold">${ad.name}</h4>
          <h5 class="card-title text-blue font-weight-bold">${ad.price}€</h5>
          <h5 class="text-blue px-2 font-weight-bold">${ad.category}</h5> 
          <p class="card-text text-blue text-truncate">${ad.description}</p>
            <a href="#" class="btn btn-search">Vedi Articolo</a>
          </div>
        </div>`

        hiddenAds.appendChild(adCard)
    })
}
mainHiddenAds(moreAds)


